FROM alpine:3.5
MAINTAINER Sander Vergeles <sander.vergeles@gmail.com>

LABEL caddy_version="0.9.5" architecture="amd64"

RUN apk add --no-cache curl

# Plugins
ARG plugins="filter,hugo,ipfilter,mailout,minify,ratelimit,realip"

# Install
RUN curl --silent --show-error --fail --location \
--header "Accept: application/tar+gzip, application/x-gzip, application/octet-stream" -o - \
"https://caddyserver.com/download/build?os=linux&arch=amd64&features=${plugins}" \
| tar --no-same-owner -C /usr/bin/ -xz caddy \
&& chmod 0755 /usr/bin/caddy \
&& /usr/bin/caddy -version

EXPOSE 80 443
WORKDIR /srv

COPY Caddyfile /etc/caddy/Caddyfile
COPY boilerplate/ /etc/caddy/boilerplate/

ENTRYPOINT ["/usr/bin/caddy"]
CMD ["--conf", "/etc/caddy/Caddyfile", "--log", "stdout"]
